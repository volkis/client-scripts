# RPC Filters

These filters are here to help prevent Coerced Authentication.

`all_filter.txt` is a combination of the rest of the, more targeted, filter files.

Run like so:

```
netsh -f ms-xxxx_filter.txt
```

## References

- <https://www.akamai.com/blog/security/guide-rpc-filter>
- <https://github.com/jsecurity101/MSRPC-to-ATTACK>
